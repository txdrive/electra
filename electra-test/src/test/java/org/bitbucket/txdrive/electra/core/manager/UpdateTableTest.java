package org.bitbucket.txdrive.electra.core.manager;

import org.bitbucket.txdrive.electra.Electra;
import org.bitbucket.txdrive.electra.TestUtils;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import java.util.Arrays;

import bean.CompIndexMetaType;
import bean.CompIndexUMetaType;
import bean.Master;
import bean.MasterMetaType;
import bean.SimpleIndexMetaType;
import bean.SimpleIndexUMetaType;
import bean.SimpleTable;
import bean.SimpleTableMetaType;
import bean.SimpleTableToCreate;
import bean.SimpleTableToCreateMetaType;
import bean.SimpleTableToUpdateMetaType;
import bean.TableInfoMetaType;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class UpdateTableTest extends AbstractRobolectricTest {
	public static final String SIMPLE_CREATED = "CREATE TABLE 'SIMPLE'('CUSTOM_NAME' TEXT NOT NULL)";
	public static final String SIMPLE_UPDATED = "CREATE TABLE 'SIMPLE'('CUSTOM_NAME' TEXT NOT NULL, 'NEW' INTEGER)";
	public static final String TO_CREATE = "CREATE TABLE 'SimpleTableToCreate'('some' TEXT)";

	public static final String SIMPLE_IDX1_UPDATED = "CREATE INDEX 'MyIndexNew' ON 'SimpleIndex' (name)";
	public static final String COMPOSITE_IDX_UPDATED = "CREATE INDEX 'c3' ON 'CompIndex' (name1, name3)";

	@Override
	public void setup() {
		helper = new Helper(RuntimeEnvironment.application, DB_NAME, 1);
		Electra.configure(Arrays.asList(new MetaType[]{
				MasterMetaType.META_TYPE,
				SimpleIndexMetaType.META_TYPE,
				CompIndexMetaType.META_TYPE,
				SimpleTableMetaType.META_TYPE}));
		em = Electra.with(helper);
	}

	@Test
	public void init() {
		Master simple = TestUtils.getMasterForTable(em, "SIMPLE");
		assertThat(simple.getSql(), is(SIMPLE_CREATED));


		Master toCreate = TestUtils.getMasterForTable(em, SimpleTableToCreate.class.getSimpleName());
		assertThat(toCreate, is(nullValue()));
	}

	@Test
	public void update() {
		em.func(SimpleTable.class).rowCount();

		Electra.configure(Arrays.asList(new MetaType[]{
				MasterMetaType.META_TYPE,
				TableInfoMetaType.META_TYPE,
				SimpleTableMetaType.META_TYPE,
				SimpleTableToUpdateMetaType.META_TYPE,
				SimpleTableToCreateMetaType.META_TYPE}));

		em.updateTables();

		Master simple = TestUtils.getMasterForTable(em, "SIMPLE");
		assertThat(simple.getSql(), is(SIMPLE_UPDATED));


		Master toCreate = TestUtils.getMasterForTable(em, SimpleTableToCreate.class.getSimpleName());
		assertThat(toCreate.getSql(), is(TO_CREATE));
	}

	@Test
	public void updateIndex() {
		Master index = TestUtils.getIndex(em, "SimpleIndex", "MyIndex");
		assertThat(index, notNullValue());

		Electra.configure(Arrays.asList(new MetaType[]{
				MasterMetaType.META_TYPE,
				SimpleIndexUMetaType.META_TYPE,
				CompIndexUMetaType.META_TYPE}));

		em.updateTables();

		index = TestUtils.getIndex(em, "SimpleIndex", "MyIndex");
		assertThat(index, nullValue());

		index = TestUtils.getIndex(em, "CompIndex", "c1");
		assertThat(index, nullValue());
		index = TestUtils.getIndex(em, "CompIndex", "c2");
		assertThat(index, nullValue());

		index = TestUtils.getIndex(em, "SimpleIndex", "MyIndexNew");
		assertThat(index.getSql(), is(SIMPLE_IDX1_UPDATED));

		index = TestUtils.getIndex(em, "CompIndex", "c3");
		assertThat(index.getSql(), is(COMPOSITE_IDX_UPDATED));
	}
}