package org.bitbucket.txdrive.electra.core.manager;


import java.util.Date;

import bean.Contact;

public class ContactBuilder {
	private Contact contact = new Contact();

	public Contact build() {
		return contact;
	}

	public ContactBuilder withName(String data) {
		contact.setName(data);
		return this;
	}

	public ContactBuilder withAddress(String data) {
		contact.setAddress(data);
		return this;
	}

	public ContactBuilder withCity(String data) {
		contact.setCity(data);
		return this;
	}

	public ContactBuilder withCountry(String data) {
		contact.setCountry(data);
		return this;
	}
	public ContactBuilder withBirthday(Date data) {
		contact.setBirthday(data);
		return this;
	}

	public ContactBuilder withCars(int data) {
		contact.setCars(data);
		return this;
	}

	public ContactBuilder withSum(float data) {
		contact.setSum(data);
		return this;
	}
}
