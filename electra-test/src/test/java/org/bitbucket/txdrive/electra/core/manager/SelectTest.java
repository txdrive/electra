package org.bitbucket.txdrive.electra.core.manager;

import org.bitbucket.txdrive.electra.core.converter.FieldConverters;
import org.bitbucket.txdrive.electra.core.query.Order;
import org.bitbucket.txdrive.electra.core.query.Properties;
import org.bitbucket.txdrive.electra.core.query.Restrictions;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import bean.Contact;
import bean.ContactCarView;
import bean.ContactSumView;
import bean.TableColumn;

import static org.bitbucket.txdrive.electra.core.query.Properties.as;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.between;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.contains;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.empty;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.eq;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.gq;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.gt;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.in;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.isNull;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.like;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.lq;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.lt;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.neq;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.not;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.notEmpty;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.notNull;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.or;
import static org.bitbucket.txdrive.electra.core.query.Restrictions.sql;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

public class SelectTest extends AbstractRobolectricTest {

	@Test
	public void selectWithoutProperties() {
		Contact first = em.select(Contact.class).first();
		assertThat(first, nullValue());

		Object[] result = em.select(Contact.class).firstResult();
		assertThat(result, nullValue());

		result = em.select(Contact.class).firstResult(new FieldConverters.LongConverter());
		assertThat(result, nullValue());

		em.create(new ContactBuilder().withName("name1").build());
		em.create(new ContactBuilder().withName("name2").build());
		List<Contact> list = em.select(Contact.class).list();
		assertThat(list, hasSize(2));

		first = em.select(Contact.class).first();
		assertThat(first, notNullValue());
		assertThat(first.getName(), is("name1"));

		result = em.select(Contact.class).firstResult();
		assertThat(result, notNullValue());
		assertThat((String)result[1], is("name1"));

		result = em.select(Contact.class).firstResult(new FieldConverters.LongConverter(),
				new FieldConverters.StringConverter());
		assertThat(result, notNullValue());
		assertThat((String)result[1], is("name1"));
	}

	@Test
	public void logical() {
		em.create(new ContactBuilder().withName("name1").withCars(1).build());
		em.create(new ContactBuilder().withName("name2").withCars(2).build());
		em.create(new ContactBuilder().withName("name3").withCars(3).build());

		List<Contact> list = em.select(Contact.class).where(eq("name", "name1")).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(or(eq("name", "name1"))).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(eq("name", "name1"), eq("cars", 1)).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(or(eq("name", "name1"), eq("cars", 2))).list();
		assertThat(list, hasSize(2));

	}


	@Test
	public void baseRestrictions() {
		em.create(new ContactBuilder().withName("name1").withCars(1).build());
		em.create(new ContactBuilder().withName("name2").withCars(2).build());
		em.create(new ContactBuilder().withName("name3").withCars(3).build());
		em.create(new ContactBuilder().withName("name4").withCars(4).build());
		em.create(new ContactBuilder().withName("name5").withCars(5).build());

		List<Contact> list = em.select(Contact.class).where(eq("name", "name1")).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(eq("name", null)).list();
		assertThat(list, hasSize(0));


		list = em.select(Contact.class).where(eq("name", 3)).list();
		assertThat(list, hasSize(0));

		list = em.select(Contact.class).where(eq("birthday", null)).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(neq("name", "name1")).list();
		assertThat(list, hasSize(4));

		list = em.select(Contact.class).where(empty("name")).list();
		assertThat(list, hasSize(0));

		list = em.select(Contact.class).where(empty("birthday")).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(notEmpty("name")).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(notEmpty("birthday")).list();
		assertThat(list, hasSize(0));


		list = em.select(Contact.class).where(contains("name", "5")).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(contains("name", "n")).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(not(contains("name", "5"))).list();
		assertThat(list, hasSize(4));

		list = em.select(Contact.class).where(like("name", "%5")).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(gt("cars", 3)).list();
		assertThat(list, hasSize(2));

		list = em.select(Contact.class).where(eq("cars",  "3")).list();
		assertThat(list, hasSize(1));

		list = em.select(Contact.class).where(eq("cars",  "a")).list();
		assertThat(list, hasSize(0));

		list = em.select(Contact.class).where(lt("cars", 5)).list();
		assertThat(list, hasSize(4));

		list = em.select(Contact.class).where(lq("cars", 3)).list();
		assertThat(list, hasSize(3));

		list = em.select(Contact.class).where(gq("cars", 4)).list();
		assertThat(list, hasSize(2));

		list = em.select(Contact.class).where(isNull("birthday")).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(isNull("name")).list();
		assertThat(list, hasSize(0));


		list = em.select(Contact.class).where(notNull("birthday")).list();
		assertThat(list, hasSize(0));

		list = em.select(Contact.class).where(notNull("name")).list();
		assertThat(list, hasSize(5));

		list = em.select(Contact.class).where(not(eq("name", "name5"))).list();
		assertThat(list, hasSize(4));

		list = em.select(Contact.class).where(between("cars", 2, 4)).list();
		assertThat(list, hasSize(3));

		list = em.select(Contact.class).where(in("cars", Arrays.asList(1,2,3))).list();
		assertThat(list, hasSize(3));

		list = em.select(Contact.class).where(sql(":name = 'name5'")).list();
		assertThat(list, hasSize(1));

		Long count = em.select(Contact.class).func().rowCount();
		assertThat(count, is(5L));


		count = em.select(Contact.class).where(eq("name", "name5")).func().rowCount();
		assertThat(count, is(1L));

		int sum = (int) em.select(Contact.class).func().sum("cars");
		assertThat(sum, is(15));

		int avg = (int) em.select(Contact.class).func().avg("cars");
		assertThat(avg, is(3));

		int max = (int) em.select(Contact.class).func().max("cars");
		assertThat(max, is(5));

		int min = (int) em.select(Contact.class).func().min("cars");
		assertThat(min, is(1));

		count = em.select(Contact.class).func().count("cars");
		assertThat(count, is(5L));

		max = (int) em.func(Contact.class).max("cars");
		assertThat(max, is(5));

	}

	@Test
	public void properties() {
		em.create(new ContactBuilder().withName("name1").withCars(1).build());
		em.create(new ContactBuilder().withName("name2").withCars(2).build());

		Contact name = em.select(Contact.class, Properties.name("name")).first();
		assertThat(name.getName(), is("name1"));
		assertThat(name.getCars(), is(0));

		name = em.select(Contact.class, Properties.name("cars")).first();
		assertThat(name.getName(),  nullValue());
		assertThat(name.getCars(), is(1));

		name = em.select(Contact.class, Properties.sql(":cars")).first();
		assertThat(name.getName(),  nullValue());
		assertThat(name.getCars(), is(0));

		name = em.select(Contact.class, as(Properties.sql(":cars"), "cars")).first();
		assertThat(name.getName(),  nullValue());
		assertThat(name.getCars(), is(1));

		name = em.select(Contact.class, Properties.max("cars")).first();
		assertThat(name.getName(),  nullValue());

		name = em.select(Contact.class, Properties.max("cars"), Properties.all()).first();
		assertThat(name.getName(), is("name2"));
		assertThat(name.getCars(), is(2));
	}

	@Test
	public void sort() {
		em.create(new ContactBuilder().withName("name1").withCars(1).build());
		em.create(new ContactBuilder().withName("name2").withCars(2).build());

		Contact name = em.select(Contact.class).order(Order.asc("name")).first();
		assertThat(name.getName(), is("name1"));

		name = em.select(Contact.class).order(Order.desc("cars")).first();
		assertThat(name.getName(), is("name2"));

		name = em.select(Contact.class).order(Order.desc("name"), Order.asc("cars")).first();
		assertThat(name.getName(), is("name2"));
	}

	@Test
	public void groupBy() {
		em.create(new ContactBuilder().withName("name1").withSum(10.f).build());
		em.create(new ContactBuilder().withName("name2").withSum(20.f).build());
		em.create(new ContactBuilder().withName("name3").withSum(30.f).build());

		List<Object[]> objects = em.select(Contact.class, Properties.name("name"), Properties.sum("sum"))
				.groupBy("name")
				.order(Order.desc("sum"))
				.listResult(new FieldConverters.StringConverter(),
						new FieldConverters.FloatConverter());

		assertThat(objects, hasSize(3));

		Object[] objects1 = objects.get(0);
		assertThat((String)objects1[0], is("name3"));
		assertThat((float)objects1[1], is(30.f));
	}

	@Test
	public void groupByUseView() {
		em.create(new ContactBuilder().withName("name1").withSum(10.f).build());
		em.create(new ContactBuilder().withName("name2").withSum(20.f).build());
		em.create(new ContactBuilder().withName("name3").withSum(30.f).build());

		ContactSumView first = em.select(ContactSumView.class, Properties.name("name"),
				as(Properties.sum("sum"), "sum"))
				.groupBy("name").order(Order.desc("sum"))
				.first();

		assertThat(first.getName(), is("name3"));
		assertThat(first.getSum(), is(30.f));
	}


	@Test
	public void havingWithView() {
		em.create(new ContactBuilder().withName("name1").withSum(10.f).build());
		em.create(new ContactBuilder().withName("name1").withSum(20.f).build());
		em.create(new ContactBuilder().withName("name2").withSum(30.f).build());
		em.create(new ContactBuilder().withName("name2").withSum(40.f).build());


		{
			List<ContactSumView> list = em.select(ContactSumView.class, Properties.name("name"),
					as(Properties.sum("sum"), "sum"))
					.having("sum(:sum) > 50")
					.groupBy("name")
					.list();

			assertThat(list, hasSize(1));

			ContactSumView contactSumView = list.get(0);

			assertThat(contactSumView.getName(), is("name2"));
			assertThat(contactSumView.getSum(), is(70.f));
		}

	}


	@Test
	public void customFields() {
		em.create(new TableColumn("name1", 30f));
		em.create(new TableColumn("name2", 40f));
		em.create(new TableColumn("name3", 50f));

		String SELECT = "select * from 'CUSTOM_TABLE' where :name = ?";


		TableColumn column = em.sqlFirstResult(TableColumn.class, SELECT, new String[]{"name1"});
		assertThat(column, notNullValue());
		assertThat(column.getSum(), is(30f));
		assertThat(column.getName(), is("name1"));

		 column = em.sqlFirstResult(TableColumn.class, SELECT, new String[]{"name1"}, Properties.name("sum"));
		assertThat(column, notNullValue());
		assertThat(column.getSum(), is(30f));
		assertThat(column.getName(), nullValue());

	}

	@Test
	public void customFields2() {
		em.create(new TableColumn("name1", 30f));
		em.create(new TableColumn("name1", 40f));
		em.create(new TableColumn("name2", 50f));
		em.create(new TableColumn("name2", 50f));

		TableColumn sum = em.select(TableColumn.class, as(Properties.sql(":sum"), "sum")).first();
		assertThat(sum, notNullValue());
		assertThat(sum.getSum(), is(30f));

	}
}
