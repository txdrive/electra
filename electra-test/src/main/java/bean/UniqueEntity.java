package bean;

import org.bitbucket.txdrive.electra.annotation.ConflictConstraint;
import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Key;
import org.bitbucket.txdrive.electra.annotation.Unique;

/**
 * Created by ancyferov on 21.12.16.
 */
@Entity
public class UniqueEntity {

    @Key
    private long id;

    @Unique
    private int uniqueNumber;

    @Unique(conflict = ConflictConstraint.REPLACE)
    private int uniqueNumberReplace;

    @Unique(conflict = ConflictConstraint.FAIL)
    private int uniqueNumberFail;

    @Unique(conflict = ConflictConstraint.IGNORE)
    private int uniqueNumberIgnore;

    @Unique(conflict = ConflictConstraint.ROLLBACK)
    private int uniqueNumberRollback;

    public UniqueEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(int uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public int getUniqueNumberRollback() {
        return uniqueNumberRollback;
    }

    public void setUniqueNumberRollback(int uniqueNumberRollback) {
        this.uniqueNumberRollback = uniqueNumberRollback;
    }

    public int getUniqueNumberReplace() {
        return uniqueNumberReplace;
    }

    public void setUniqueNumberReplace(int uniqueNumberReplace) {
        this.uniqueNumberReplace = uniqueNumberReplace;
    }

    public int getUniqueNumberFail() {
        return uniqueNumberFail;
    }

    public void setUniqueNumberFail(int uniqueNumberFail) {
        this.uniqueNumberFail = uniqueNumberFail;
    }

    public int getUniqueNumberIgnore() {
        return uniqueNumberIgnore;
    }

    public void setUniqueNumberIgnore(int uniqueNumberIgnore) {
        this.uniqueNumberIgnore = uniqueNumberIgnore;
    }
}
