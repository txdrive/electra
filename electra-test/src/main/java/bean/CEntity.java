package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity
public class CEntity extends SEntity {
	private byte field;

	public byte getField() {
		return field;
	}

	public void setField(byte field) {
		this.field = field;
	}
}
