package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity(value = "contact", schema = {})
public class ContactCarView {
	private String name;
	private int cars;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCars() {
		return cars;
	}

	public void setCars(int cars) {
		this.cars = cars;
	}
}
