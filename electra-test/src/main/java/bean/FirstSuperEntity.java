package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Key;
import org.bitbucket.txdrive.electra.annotation.SuperEntity;

@SuperEntity
abstract public class FirstSuperEntity {
	@Key
	private long id;

	@Column("ACTIVE")
	private String activeFlag;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
}
