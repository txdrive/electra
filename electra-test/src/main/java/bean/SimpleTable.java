package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Index;
import org.bitbucket.txdrive.electra.annotation.NotNull;
import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.annotation.SuperEntity;

@Entity(value = "SIMPLE", schema = Schema.CREATE)
@SuperEntity
public class SimpleTable {
	@Column("CUSTOM_NAME")
	@Index("IDX")
	@NotNull
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
