package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Schema;

@Entity(value = "SIMPLE", schema = Schema.UPDATE)
public class SimpleTableToUpdate extends SimpleTable {
	@Column("NEW")
	private int newField;

	public int getNewField() {
		return newField;
	}

	public void setNewField(int newField) {
		this.newField = newField;
	}
}
