package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Ignore;
import org.bitbucket.txdrive.electra.annotation.Key;
import org.bitbucket.txdrive.electra.annotation.filter.PostCreate;
import org.bitbucket.txdrive.electra.annotation.filter.PostDelete;
import org.bitbucket.txdrive.electra.annotation.filter.PostRead;
import org.bitbucket.txdrive.electra.annotation.filter.PostUpdate;
import org.bitbucket.txdrive.electra.annotation.filter.PreCreate;
import org.bitbucket.txdrive.electra.annotation.filter.PreDelete;
import org.bitbucket.txdrive.electra.annotation.filter.PreUpdate;

@Entity
public class LCEntity {
	@Key
	private long id;

	private String name;

	@Ignore
	private boolean postRead;
	@Ignore
	private boolean preCreate;
	@Ignore
	private boolean postCreate;
	@Ignore
	private boolean preUpdate;
	@Ignore
	private boolean postUpdate;
	@Ignore
	private boolean preDelete;
	@Ignore
	private boolean postDelete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@PostRead
	public void postRead() {
		postRead = true;
	}

	@PreCreate
	public void preCreate() {
		preCreate = true;
	}

	@PostCreate
	public void postCreate() {
		postCreate = true;
	}

	@PreUpdate
	public void preUpdate() {
		preUpdate = true;
	}

	@PostUpdate
	public void postUpdate() {
		postUpdate = true;
	}

	@PreDelete
	public void preDelete() {
		preDelete = true;
	}

	@PostDelete
	public void postPostDelete() {
		postDelete = true;
	}

	public boolean isPostRead() {
		return postRead;
	}

	public boolean isPreCreate() {
		return preCreate;
	}

	public boolean isPostCreate() {
		return postCreate;
	}

	public boolean isPreUpdate() {
		return preUpdate;
	}

	public boolean isPostUpdate() {
		return postUpdate;
	}

	public boolean isPreDelete() {
		return preDelete;
	}

	public boolean isPostDelete() {
		return postDelete;
	}

	public void reset() {
		postRead = preCreate = postCreate = preUpdate = postUpdate = preDelete = postDelete = false;
	}
}
