package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity(schema = {})
final public class TableInfo {
	private String name;
	private String type;
	private int notnull;

	public TableInfo() {
	}

	public TableInfo(String name, String type, int notnull) {
		this.name = name;
		this.type = type;
		this.notnull = notnull;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getNotnull() {
		return notnull;
	}

	public void setNotnull(int notnull) {
		this.notnull = notnull;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TableInfo tableInfo = (TableInfo) o;

		if (notnull != tableInfo.notnull) return false;
		if (name != null ? !name.equals(tableInfo.name) : tableInfo.name != null) return false;
		return type != null ? type.equals(tableInfo.type) : tableInfo.type == null;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + notnull;
		return result;
	}
}
