package bean;

import org.bitbucket.txdrive.electra.annotation.SuperEntity;

@SuperEntity
abstract public class SEntity extends S0Entity {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
