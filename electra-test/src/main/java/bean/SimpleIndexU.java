package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Index;
import org.bitbucket.txdrive.electra.annotation.Schema;

@Entity(value = "SimpleIndex", schema = {Schema.UPDATE})
public class SimpleIndexU extends SimpleIndex {
	@Index("MyIndexNew")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
