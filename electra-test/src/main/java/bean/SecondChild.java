package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity
public class SecondChild extends SecondSuperEntity {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
