package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity(value = "sqlite_master" , schema = {})
public class Master {
	private String type;
	private String name;
	private String tbl_name;
	private String sql;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTbl_name() {
		return tbl_name;
	}

	public void setTbl_name(String tbl_name) {
		this.tbl_name = tbl_name;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
