package org.bitbucket.txdrive.electra.annotation;

public enum ConflictConstraint {
    ROLLBACK, ABORT, FAIL, IGNORE, REPLACE
}
