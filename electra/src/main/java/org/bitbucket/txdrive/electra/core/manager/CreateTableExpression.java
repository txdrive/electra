/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import org.bitbucket.txdrive.electra.annotation.ConflictConstraint;
import org.bitbucket.txdrive.electra.core.query.SqlQuery;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaUtils;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

import java.util.List;

public class CreateTableExpression implements SqlQuery {
	private MetaType<?> metaType;

	public CreateTableExpression(MetaType metaType) {
		this.metaType = metaType;
	}

	@Override
	public String toSql() {
		StringBuilder sql = new StringBuilder();

		String tableName = metaType.getTableName();
		List<MetaField> keyFields = MetaUtils.getKeyFields(metaType);
		MetaUtils.validateKeyFields(metaType.getName(), keyFields);


		sql.append("CREATE TABLE IF NOT EXISTS ")
				.append(SqlUtils.quote(tableName))
				.append("(");

		for (MetaField metaField : MetaUtils.getEditFields(metaType)) {
			sql.append(SqlUtils.quote(metaField.getColumnName()))
					.append(" ").append(metaField.getConverter().getType().toString());

			processSinglePrimaryKey(sql, keyFields, metaField);

			if (metaField.isNotNull()) {
				sql.append(" NOT NULL");
			}

			if (metaField.isUnique()) {
				sql.append(" UNIQUE");
			}

			ConflictConstraint conflictConstraint = metaField.getConflictConstraint();
			if (conflictConstraint != null) {
				sql.append(" ON CONFLICT ").append(conflictConstraint);
			}

			sql.append(", ");
		}

		sql.delete(sql.lastIndexOf(", "), sql.length());

		processMultiplePrimaryKeys(sql, keyFields);

		sql.append(");");

		return sql.toString();
	}

	private void processMultiplePrimaryKeys(StringBuilder sql, List<MetaField> keyFields) {
		if (keyFields.size() > 1) {
			sql.append(", PRIMARY KEY(");

			for (MetaField kf : keyFields) {
				sql.append(kf.getColumnName()).append(", ");
			}

			sql.delete(sql.lastIndexOf(", "), sql.length());
			sql.append(")");
		}
	}

	private void processSinglePrimaryKey(StringBuilder sql, List<MetaField> keyFields, MetaField metaField) {
		if (keyFields.size() == 1) {
			if (metaField.isKey()) {
				sql.append(" PRIMARY KEY");

				if (metaField.isAuto()) {
					sql.append(" AUTOINCREMENT");
				}
			}
		}
	}
}
