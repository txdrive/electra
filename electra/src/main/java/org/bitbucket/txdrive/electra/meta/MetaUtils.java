/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.meta;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.core.converter.ColumnType;
import org.bitbucket.txdrive.electra.core.converter.FieldConverter;
import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

public class MetaUtils {

	public static List<MetaField> getKeyFields(MetaType<?> metaType) {
		List<MetaField> result = new ArrayList<>();

		for (MetaField metaField : metaType.getMetaFields()) {
			if (metaField.isKey()) {
				result.add(metaField);
			}
		}

		return result;
	}

	public static MetaField findMetaField(MetaType<?> metaType, String name) {
		MetaField result = null;

		for (MetaField metaField : metaType.getMetaFields()) {
			if (metaField.getName().equals(name)) {
				result = metaField;
				break;
			}
		}

		return result;
	}

	public static List<MetaField> getEditFields(MetaType<?> metaType) {
		List<MetaField> result = new ArrayList<>();

		for (MetaField metaField : metaType.getMetaFields()) {
			if (!metaField.isReadOnly()) {
				result.add(metaField);
			}
		}

		return result;
	}

	public static List<MetaField> getEditFieldsSkipKeyFields(MetaType<?> metaType) {
		List<MetaField> result = new ArrayList<>();

		List<MetaField> editFields = getEditFields(metaType);

		for (MetaField metaField : editFields) {
			if (!metaField.isKey()) {
				result.add(metaField);
			}
		}

		return result;
	}

	public static void validateKeyFields(String typeName, List<MetaField> metaFields) {
		boolean multipleKeys = metaFields.size() > 1;

		for (MetaField metaField : metaFields) {
			if (metaField.isAuto()) {
				if (multipleKeys) {
					throw new ElectraException(typeName
							+ ": Auto increment can be set only if there is a singe key");
				}

				FieldConverter converter = metaField.getConverter();
				if (!converter.getType().equals(ColumnType.INTEGER)) {
					throw new ElectraException(typeName + ": " + metaField.getName()
							+ ": Auto increment column type must have db type 'INTEGER'");
				}
			}
		}
	}

	public static boolean isSchemaPresent(Schema[] schemas, Schema schemaToCheck) {
		boolean result = false;

		for (Schema schema : schemas) {
			if (schema.equals(schemaToCheck)) {
				result = true;
				break;
			}
		}

		return result;
	}

	public static boolean isEmptyKeyValues(List<Object> values) {
		boolean result = true;

		for (Object value : values) {
			if (value == null) {
				result = false;
				break;
			}
		}

		return result;
	}

	public static <T> T convert(MetaType<T> metaType, List<MetaField<T, ?>> metaFields, Cursor cursor) {

		T entity = metaType.getCreator().create();
		for (MetaField metaField : metaFields) {
			int columnIndex = cursor.getColumnIndex(metaField.getColumnName());
			if (columnIndex > -1) {
				FieldConverter fieldConverter = metaField.getConverter();
				Object value = fieldConverter.fromCursor(cursor, columnIndex);
				FieldAccessor fieldAccessor = metaField.getAccessor();
				fieldAccessor.set(entity, value);
			}
		}

		return entity;
	}

	public static <V, T> List<V> extractFieldValues(Class<T> type, List<T> entities, String name) {
		MetaType<T> metaType = MetaTypes.typeOf(type);

		List<V> values = new ArrayList<>();

		MetaField<T, ?> metaField = SqlUtils.getMetaFieldByName(metaType.getMetaFields(), name);
		FieldAccessor<T, ?> accessor = metaField.getAccessor();

		for (T entity : entities) {
			values.add((V) accessor.get(entity));
		}

		return values;
	}

	public static <T> Object[] getFieldValues(T entity, List<MetaField> metaFields) {
		Object[] objects = new Object[metaFields.size()];

		int idx = 0;
		for (MetaField metaField : metaFields) {
			FieldAccessor accessor = metaField.getAccessor();
			objects[idx] = accessor.get(entity);
			idx++;
		}

		return objects;
	}

}
