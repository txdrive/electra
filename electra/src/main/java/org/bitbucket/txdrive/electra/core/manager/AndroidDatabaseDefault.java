/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AndroidDatabaseDefault implements ElectraDatabase {
	private SQLiteDatabase database;

	public AndroidDatabaseDefault(SQLiteDatabase database) {
		this.database = database;
	}

	@Override
	public void execSQL(String sql) {
		database.execSQL(sql);
	}

	@Override
	public Cursor rawQuery(String sql, String[] selectionArgs) {
		return database.rawQuery(sql, selectionArgs);
	}

	@Override
	public long insert(String table, ContentValues cv) {
		return database.insert(table, null, cv);
	}

	@Override
	public long update(String table, ContentValues cv, String where) {
		return database.update(table, cv, where, null);
	}

	@Override
	public long delete(String table, String where) {
		return database.delete(table, where, null);
	}

	@Override
	public void beginTransaction() {
		database.beginTransaction();
	}

	@Override
	public void setTransactionSuccessful() {
		database.setTransactionSuccessful();
	}

	@Override
	public void endTransaction() {
		database.endTransaction();
	}

	@Override
	public void close() {
		database.close();
	}

	@Override
	public boolean isOpen() {
		return database.isOpen();
	}

	@Override
	public int getVersion() {
		return database.getVersion();
	}

	@Override
	public Object getWrappedDatabase() {
		return database;
	}
}
