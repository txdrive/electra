/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import android.database.Cursor;

import java.util.List;

import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaUtils;
import org.bitbucket.txdrive.electra.utils.SqlUtils;


public class EntityCursor<T> {
	private Cursor cursor;
	private MetaType<T> metaType;
	private List<MetaField<T, ?>> metaFields;

	public EntityCursor(Cursor cursor, MetaType<T> metaType, Property[] properties) {
		this.cursor = cursor;
		this.metaType = metaType;
		this.metaFields = SqlUtils.filterByProperties(metaType.getMetaFields(), properties);
	}

	public Cursor getCursor() {
		return cursor;
	}

	public int count() {
		return cursor.getCount();
	}

	public boolean moveToPosition(int position) {
		return cursor.moveToPosition(position);
	}

	public int getPosition(int position) {
		return cursor.getPosition();
	}

	public boolean moveToNext() {
		return cursor.moveToNext();
	}

	public boolean moveToPrevious() {
		return cursor.moveToPrevious();
	}

	public boolean moveToFirst() {
		return cursor.moveToFirst();
	}

	public boolean moveToLast() {
		return cursor.moveToLast();
	}

	public void close() {
		cursor.close();
	}

	public T get() {
		T entity = MetaUtils.convert(metaType, metaFields, cursor);
		metaType.notifyPostRead(entity);

		return entity;
	}
}
