/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler;

import org.bitbucket.txdrive.electra.annotation.ConflictConstraint;
import org.bitbucket.txdrive.electra.meta.IndexMeta;

import javax.lang.model.element.VariableElement;


public class FieldInfo {
	private VariableElement element;
	private String name;
	private String columnName;
	private boolean isKey;
	private boolean auto;
	private boolean unique;
	private ConflictConstraint conflictConstraint;
	private boolean notNull;
	private boolean readOnly;
	private String converter;
	private String getter;
	private String setter;
	private IndexMeta index;

	public FieldInfo(VariableElement element) {
		this.element = element;
	}

	public VariableElement getElement() {
		return element;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public void setElement(VariableElement element) {
		this.element = element;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public boolean isKey() {
		return isKey;
	}

	public void setKey(boolean key) {
		isKey = key;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}

	public String getGetter() {
		return getter;
	}

	public void setGetter(String getter) {
		this.getter = getter;
	}

	public String getSetter() {
		return setter;
	}

	public void setSetter(String setter) {
		this.setter = setter;
	}

	public String getConverter() {
		return converter;
	}

	public void setConverter(String converter) {
		this.converter = converter;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public IndexMeta getIndex() {
		return index;
	}

	public void setIndex(IndexMeta index) {
		this.index = index;
	}

	public ConflictConstraint getConflictConstraint() {
		return conflictConstraint;
	}

	public void setConflictConstraint(ConflictConstraint conflictConstraint) {
		this.conflictConstraint = conflictConstraint;
	}
}
