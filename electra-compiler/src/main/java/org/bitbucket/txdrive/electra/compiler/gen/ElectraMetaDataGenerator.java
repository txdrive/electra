/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler.gen;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;

import org.bitbucket.txdrive.electra.compiler.EntityInfo;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.meta.MetaTypes;

public class ElectraMetaDataGenerator extends AbstractCodeGenerator {

	private final List<EntityInfo> entityInfoList;
	private String descName;

	public ElectraMetaDataGenerator(ProcessingEnvironment pe,
	                                List<EntityInfo> entityInfoList,
	                                String descName) {
		super(pe);
		this.entityInfoList = entityInfoList;
		this.descName = descName;
	}

	@Override
	protected JavaFile buildJavaFile() {
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(descName);
		typeBuilder.addModifiers(Modifier.PUBLIC);

		FieldSpec.Builder metaTypesFieldBuilder = generateTypes();
		typeBuilder.addField(metaTypesFieldBuilder.build());

		return JavaFile.builder("electra", typeBuilder.build()).build();
	}

	private FieldSpec.Builder generateTypes() {
		ParameterizedTypeName paramTypeName = ParameterizedTypeName
				.get(ClassName.get(List.class), ClassName.get(MetaType.class));


		FieldSpec.Builder metaTypesFieldBuilder = FieldSpec.builder(paramTypeName, "TYPES",
				Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);


		ClassName metaTypesBuilderClassName = ClassName.get(MetaTypes.ElectraMetaTypesBuilder.class);
		CodeBlock.Builder metaFieldInit = CodeBlock.builder()
				.add("new $T()\n", metaTypesBuilderClassName);

		for (EntityInfo entityInfo : entityInfoList) {
			ClassName className = ClassName.bestGuess(entityInfo.getElementType()
					.getQualifiedName()
					+ MetaTypeGenerator.META_TYPE_CLASS_PREFIX);
			metaFieldInit.add(".withMetaType($T."
					+ MetaTypeGenerator.META_TYPE_FIELD + ")\n", className);
		}

		metaFieldInit.add(".build()");

		metaTypesFieldBuilder.initializer("$L", metaFieldInit.build());
		return metaTypesFieldBuilder;
	}
}
