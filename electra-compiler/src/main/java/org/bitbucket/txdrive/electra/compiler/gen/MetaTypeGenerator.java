/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler.gen;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import org.bitbucket.txdrive.electra.annotation.ConflictConstraint;
import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.compiler.EntityInfo;
import org.bitbucket.txdrive.electra.compiler.FieldInfo;
import org.bitbucket.txdrive.electra.compiler.ProcessorUtils;
import org.bitbucket.txdrive.electra.meta.EntityCreator;
import org.bitbucket.txdrive.electra.meta.EntityFilterAdapter;
import org.bitbucket.txdrive.electra.meta.FieldAccessor;
import org.bitbucket.txdrive.electra.meta.IndexMeta;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

public class MetaTypeGenerator extends AbstractCodeGenerator {
	public static final String META_TYPE_FIELD = "META_TYPE";
	public static final String META_TYPE_CLASS_PREFIX = "MetaType";

	private EntityInfo entityInfo;

	private static Map<TypeKind, Class> primitiveTypes = new HashMap<>();

	private List<String> fieldNames = new ArrayList<>();

	static {
		primitiveTypes.put(TypeKind.BYTE, Byte.class);
		primitiveTypes.put(TypeKind.SHORT, Short.class);
		primitiveTypes.put(TypeKind.INT, Integer.class);
		primitiveTypes.put(TypeKind.LONG, Long.class);
		primitiveTypes.put(TypeKind.FLOAT, Float.class);
		primitiveTypes.put(TypeKind.DOUBLE, Double.class);
		primitiveTypes.put(TypeKind.BOOLEAN, Boolean.class);
	}

	public MetaTypeGenerator(ProcessingEnvironment pe, EntityInfo entityInfo) {
		super(pe);
		this.entityInfo = entityInfo;
	}

	@Override
	protected JavaFile buildJavaFile() {
		ClassName entityClassName = ClassName.get(entityInfo.getElementType());
		TypeName entityTypeName = TypeName.get(entityInfo.getElementType().asType());

		PackageElement packageName = elementsUtils.getPackageOf(entityInfo.getElementType());

		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(entityInfo.getName()
				+ META_TYPE_CLASS_PREFIX);
		typeBuilder.addModifiers(Modifier.PUBLIC);

		generateMetaField(typeBuilder, entityTypeName);
		generateMetaTypeField(typeBuilder, entityClassName, entityTypeName);

		return JavaFile.builder(packageName.toString(), typeBuilder.build()).build();
	}

	private TypeName getTypeName(TypeMirror typeMirror) {
		TypeName typeName = null;
		if (typeMirror.getKind().isPrimitive()) {
			typeName = TypeName.get(primitiveTypes.get(typeMirror.getKind()));
		}

		if (typeName == null) {
			typeName = TypeName.get(typeMirror);
		}


		return typeName;
	}

	private void generateMetaField(TypeSpec.Builder typeBuilder, TypeName entityTypeName) {
		for (FieldInfo fieldInfo : entityInfo.getFieldInfoLis()) {
			if (!ProcessorUtils.isEmpty(fieldInfo.getConverter())) {
				processBasicField(typeBuilder, entityTypeName, fieldInfo);
			}
		}
	}

	private void processBasicField(TypeSpec.Builder typeBuilder,
	                               TypeName entityTypeName,
	                               FieldInfo fieldInfo) {

		String fieldName = fieldInfo.getName().toUpperCase();

		TypeName fieldTypeName = getTypeName(fieldInfo.getElement().asType());

		ParameterizedTypeName paramTypeName = ParameterizedTypeName
				.get(ClassName.get(MetaField.class), entityTypeName, fieldTypeName);

		FieldSpec.Builder metaFieldBuilder = FieldSpec.builder(paramTypeName,
				fieldName,
				Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);

		ClassName metaFieldBuilderClassName = ClassName.get(MetaField.MetaFieldBuilder.class);

		ParameterizedTypeName paramAccessor = ParameterizedTypeName
				.get(ClassName.get(FieldAccessor.class), entityTypeName, fieldTypeName);

		TypeSpec.Builder fieldAccessor = TypeSpec.anonymousClassBuilder("")
				.addSuperinterface(paramAccessor);

		MethodSpec setter = MethodSpec.methodBuilder("set")
				.addParameter(entityTypeName, "entity")
				.addParameter(fieldTypeName, "value")
				.addModifiers(Modifier.PUBLIC)
				.addAnnotation(Override.class)
				.addStatement("entity.$L(value)", fieldInfo.getSetter())
				.build();
		fieldAccessor.addMethod(setter);

		MethodSpec getter = MethodSpec.methodBuilder("get")
				.addParameter(entityTypeName, "entity")
				.addModifiers(Modifier.PUBLIC)
				.returns(fieldTypeName)
				.addAnnotation(Override.class)
				.addStatement("return entity.$L()", fieldInfo.getGetter())
				.build();
		fieldAccessor.addMethod(getter);


		CodeBlock.Builder metaFieldInit = CodeBlock.builder()
				.add("new $T<$T,$T>()\n", metaFieldBuilderClassName, entityTypeName, fieldTypeName)
				.add(".withType($T.class)\n", fieldTypeName)
				.add(".withName($S)\n", fieldInfo.getName());

		if (fieldInfo.getColumnName() != null
				&& !fieldInfo.getColumnName().trim().equals("")) {
			metaFieldInit.add(".withColumnName($S)\n", fieldInfo.getColumnName());
		}

		if (fieldInfo.isKey()) {
			metaFieldInit.add(".withKey($L)\n", fieldInfo.isKey());
		}

		if (fieldInfo.isAuto()) {
			metaFieldInit.add(".withAuto($L)\n", fieldInfo.isAuto());
		}

		if (fieldInfo.isNotNull()) {
			metaFieldInit.add(".withNotNull($L)\n", fieldInfo.isNotNull());
		}

		if (fieldInfo.isUnique()) {
			metaFieldInit.add(".withUnique($L)\n", fieldInfo.isUnique());
			metaFieldInit.add(".withConflictConstraint($T.$L)\n", ConflictConstraint.class, fieldInfo.getConflictConstraint());
		}

		if (fieldInfo.isReadOnly()) {
			metaFieldInit.add(".withReadOnly($L)\n", fieldInfo.isReadOnly());
		}

		if (fieldInfo.getIndex() != null) {
			ClassName indexMetaClassName = ClassName.get(IndexMeta.class);
			String indexName = fieldInfo.getIndex().getName();
			metaFieldInit.add(".withIndex(new $T($S))\n", indexMetaClassName, indexName);
		}


		metaFieldInit.add(".withAccessor($L)\n", fieldAccessor.build());

		if (fieldInfo.getConverter() != null) {
			ClassName converterClassName = ClassName.bestGuess(fieldInfo.getConverter());

			metaFieldInit.add(".withConverter(new $T())\n", converterClassName);
		}

		metaFieldInit.add(".build()");

		metaFieldBuilder.initializer("$L", metaFieldInit.build());

		typeBuilder.addField(metaFieldBuilder.build());
		fieldNames.add(fieldName);
	}

	private void generateMetaTypeField(TypeSpec.Builder typeBuilder,
	                                   ClassName entityClassName,
	                                   TypeName entityTypeName) {

		ParameterizedTypeName paramTypeName = ParameterizedTypeName
				.get(ClassName.get(MetaType.class), entityTypeName);

		FieldSpec.Builder metaTypeFieldBuilder = FieldSpec.builder(paramTypeName, META_TYPE_FIELD,
				Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);

		ParameterizedTypeName paramCreatorTypeName = ParameterizedTypeName
				.get(ClassName.get(EntityCreator.class), entityTypeName);

		TypeSpec.Builder typeCreator = TypeSpec.anonymousClassBuilder("")
				.addSuperinterface(paramCreatorTypeName);


		MethodSpec createTypeMethod = MethodSpec.methodBuilder("create")
				.addModifiers(Modifier.PUBLIC)
				.addAnnotation(Override.class)
				.addStatement("return new $T()", entityTypeName)
				.returns(entityTypeName)
				.build();
		typeCreator.addMethod(createTypeMethod);

		ClassName entityMetaTypeBuilderClassName = ClassName.get(MetaType.MetaTypeBuilder.class);

		CodeBlock.Builder metaTypeInit = CodeBlock.builder()
				.add("new $T<$T>()\n", entityMetaTypeBuilderClassName, entityClassName)
				.add(".withType($T.class)\n", entityClassName)
				.add(".withName($S)\n", entityInfo.getName())
				.add(".withCreator($L)\n", typeCreator.build());

		if (!StringUtils.isEmpty(entityInfo.getTableName())) {
			metaTypeInit.add(".withTableName($S)\n", entityInfo.getTableName());
		}

		processMetaTypeSchema(metaTypeInit);

		generateFilterClass(metaTypeInit, entityTypeName);

		for (String fieldName : fieldNames) {
			metaTypeInit.add(".withMetaField($L)\n", fieldName);
		}

		for (IndexMeta indexMeta : entityInfo.getIndexes()) {
			ClassName indexMetaClassName = ClassName.get(IndexMeta.class);
			String indexName = indexMeta.getName();
			String[] columNames = indexMeta.getColumnNames();

			if (columNames != null && columNames.length > 0) {
				String[] wrawp = StringUtils.wrap(columNames);
				String columnNamesString = StringUtils.join(Arrays.asList(wrawp), ", ");


				metaTypeInit.add(".withIndex(new $T($S, $L))\n", indexMetaClassName, indexName,
						columnNamesString);
			} else {
				metaTypeInit.add(".withIndex(new $T($S))\n", indexMetaClassName, indexName);
			}
		}

		metaTypeInit.add(".build()");

		metaTypeFieldBuilder.initializer("$L", metaTypeInit.build());
		typeBuilder.addField(metaTypeFieldBuilder.build());
	}

	private void processMetaTypeSchema(CodeBlock.Builder metaTypeInit) {
		Schema[] schema = entityInfo.getSchema();

		if (schema.length > 0) {
			ClassName schemaClass = ClassName.get(Schema.class);

			List<String> values = new ArrayList<>();

			Object[] args = new Object[schema.length * 2];
			int idx = 0;
			for (Schema sc : schema) {
				values.add("$T.$L");
				args[idx++] = schemaClass;
				args[idx++] = sc.toString();
			}

			metaTypeInit.add(".withSchema(" + StringUtils.join(values, ", ") + ")\n", args);
		}
	}

	private void generateFilterClass(CodeBlock.Builder metaTypeInit,
	                                 TypeName entityTypeName) {

		ParameterizedTypeName filterParams = ParameterizedTypeName
				.get(ClassName.get(EntityFilterAdapter.class), entityTypeName);

		TypeSpec.Builder filterTypeBuilder = TypeSpec.anonymousClassBuilder("")
				.addSuperinterface(filterParams);


		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPreCreateMethodNames(), "preCreate");
		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPostCreateMethodNames(), "postCreate");

		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPostReadMethodNames(), "postRead");

		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPreUpdateMethodNames(), "preUpdate");
		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPostUpdateMethodNames(), "postUpdate");

		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPreDeleteMethodNames(), "preDelete");

		generateFilterMethod(filterTypeBuilder, entityTypeName,
				entityInfo.getPostDeleteMethodNames(), "postDelete");

		metaTypeInit.add(".withFilter($L)\n", filterTypeBuilder.build());
	}

	private void generateFilterMethod(TypeSpec.Builder filterTypeBuilder,
	                                  TypeName entityTypeName,
	                                  List<String> filterMethodNames, String method) {
		if (filterMethodNames.size() > 0) {
			MethodSpec.Builder filterMethodBuilder = MethodSpec.methodBuilder(method)
					.addModifiers(Modifier.PUBLIC)
					.addParameter(entityTypeName, "entity")
					.addAnnotation(Override.class);

			for (String name : filterMethodNames) {
				filterMethodBuilder.addStatement("entity.$L()", name);
			}

			filterTypeBuilder.addMethod(filterMethodBuilder.build());
		}
	}

}
